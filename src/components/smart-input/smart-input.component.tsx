import React, { Component } from "react";
import { Input, Row, Col, Button } from "reactstrap";

interface Props {
  value?: number | string;
  type?: any;
  onSave?: any;
}
interface State {
  value: number | string | undefined;
  edit: boolean;
}

export default class SmartInput extends Component<Props, State> {
  private _value: number | string | undefined;

  constructor(props: any) {
    super(props);

    this.state = {
      value: "",
      edit: false
    };
  }

  public componentDidMount(): void {
    this.updateState(this.props);
  }

  public componentWillReceiveProps(nextProps: any): void {
    this.updateState(nextProps);
  }

  private updateState(data: any): void {
    this.setState({
      value: data.value
    });
  }

  private onSave(): void {
    this.setState({ edit: false });
    this.props.onSave(this.state.value);
  }

  public render(): JSX.Element {
    return (
      <div className="mb-3">
        <Row>
          <Col xs="8">
            {this.state.edit ? (
              <Input
                type={this.props.type}
                value={this.state.value}
                onChange={e => {
                  this.setState({ value: e.target.value });
                  // this.props.onSave(e.target.value);
                }}
              />
            ) : (
              <p>{this.state.value}</p>
            )}
          </Col>
          <Col xs="4">
            {this.state.edit ? (
              <div>
                <Button
                  className="mr-2"
                  color="success"
                  onClick={() => this.onSave()}
                >
                  <i className="fa fa-save"></i>
                </Button>
                <Button
                  color="danger"
                  onClick={() =>
                    this.setState({ edit: false, value: this._value })
                  }
                >
                  <i className="fa fa-ban"></i>
                </Button>
              </div>
            ) : (
              <Button
                color="primary"
                onClick={() => {
                  this.setState({ edit: true });
                  this._value = this.state.value;
                }}
              >
                <i className="fa fa-pencil"></i>
              </Button>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}
