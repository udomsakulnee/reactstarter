import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
// import { renderRoutes } from 'react-router-config';
import "./App.scss";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Containers
const DefaultLayout = React.lazy(() =>
  import("./containers/DefaultLayout/DefaultLayout")
);

// Pages
const Login = React.lazy(() => import("./views/Pages/Login/Login"));
const Register = React.lazy(() => import("./views/Pages/Register/Register"));
const Page404 = React.lazy(() => import("./views/Pages/Page404/Page404"));
const Page500 = React.lazy(() => import("./views/Pages/Page500/Page500"));

interface State {
  dynamicNum: number,
  message: string
}

class App extends Component<{}, State> {
  private text: string = "";
  private num: number = 0;

  constructor(props: any) {
    super(props);

    this.text = "ตัวอย่างข้อความ";
    this.state = {
      dynamicNum: 0,
      message: "ข้อความ"
    }

    setInterval(() => {
      this.num++;
      console.log(this.num);
    }, 1000);
  }

  public increseNum(): void {
    alert('increse');
    this.setState({
      dynamicNum: this.state.dynamicNum + 1
    });
  }

  render() {
    return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route exact path="/login" render={props => <Login {...props}/>} />
              <Route exact path="/register" render={props => <Register {...props}/>} />
              <Route exact path="/404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" render={props => <Page500 {...props}/>} />
              <Route path="/" render={props => <DefaultLayout {...props}/>} />
            </Switch>
          </React.Suspense>
      </HashRouter>
      // <div>
      //   <h1>Show Message</h1>
      //   <p>{this.text}</p>
      //   <p>Var {this.num}</p>
      //   <p>State {this.state.dynamicNum}</p>
      //   <button onClick={() => this.increseNum()}>Click</button>
      // </div>
    );
  }
}

export default App;
