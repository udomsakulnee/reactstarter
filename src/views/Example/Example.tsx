import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Alert
} from "reactstrap";
import SmartInput from "../../components/smart-input/smart-input.component";

interface State {
  email: string;
  password: string;
}

export default class Example extends Component<{}, State> {
  private alertType: string = "";

  constructor(props: any) {
    super(props);

    this.alertType = "danger";

    this.state = {
      email: "example@email.com",
      password: ""
    };
  }

  public reset(): void {
    this.setState({
      email: "",
      password: ""
    });
  }

  public onSubmit(): void {
    alert("Email: " + this.state.email + " Pass: " + this.state.password);
  }

  render() {
    return (
      <div>
        <Card>
          <CardHeader>
            <strong>Horizontal</strong> Form
          </CardHeader>
          <CardBody>
            <Alert color={this.alertType}>
              Email: {this.state.email} Password: {this.state.password}
            </Alert>
            <Form action="" method="post" className="form-horizontal">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="hf-email">Email</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    type="email"
                    id="hf-email"
                    name="hf-email"
                    placeholder="Enter Email..."
                    autoComplete="email"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                  />
                  <FormText className="help-block">
                    Please enter your email
                  </FormText>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="hf-password">Password</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    type="password"
                    id="hf-password"
                    name="hf-password"
                    placeholder="Enter Password..."
                    autoComplete="current-password"
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                  />
                  <FormText className="help-block">
                    Please enter your password
                  </FormText>
                </Col>
              </FormGroup>
            </Form>
          </CardBody>
          <CardFooter>
            <Button
              type="submit"
              size="sm"
              color="primary"
              onClick={() => this.onSubmit()}
            >
              <i className="fa fa-dot-circle-o"></i> Submit
            </Button>
            <Button
              type="reset"
              size="sm"
              color="danger"
              onClick={() => this.reset()}
            >
              <i className="fa fa-ban"></i> Reset
            </Button>
          </CardFooter>
        </Card>

        <Card>
          <CardHeader>
            <strong>Custom</strong> Component
          </CardHeader>
          <CardBody>
            <SmartInput type="text" value="ทดสอบ" onSave={(value: any) => alert(value)} />
            <SmartInput type="number" value={123} />
            <SmartInput value={this.state.email} />
            <SmartInput value="ข้อความ" />
            <SmartInput />
          </CardBody>
          <CardFooter></CardFooter>
        </Card>
      </div>
    );
  }
}
